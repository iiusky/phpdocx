<?php
/**
 * Created by PhpStorm.
 * User: lichao
 * Date: 2017/8/31
 * Time: 13:46
 */


require_once 'Classes/Phpdocx/Create/CreateDocx.inc';
$save_name         = 'anbai_report_' . time();
$tmp_save_dir_name = time();
$tmp_save_name     = '/tmp/word/' . $tmp_save_dir_name . '/success_' . $save_name;
$zip_file_name     = 'anbai_report_' . time() . '.zip';
if (!file_exists('/tmp/word/')) {
	mkdir("/tmp/word/");
}
if (!file_exists('/tmp/word/' . $tmp_save_dir_name . '/')) {
	mkdir('/tmp/word/' . $tmp_save_dir_name . '/');
}

if (!file_exists('/tmp/word/' . $tmp_save_dir_name . '/success/')) {
	mkdir('/tmp/word/' . $tmp_save_dir_name . '/success/');
}

/**
 * 删除目录及目录下所有文件或删除指定文件
 * @param str $path 待删除目录路径
 * @param int $delDir 是否删除目录，1或true删除目录，0或false则只删除文件保留目录（包含子目录）
 * @return bool 返回删除状态
 */
function delDirAndFile($path, $delDir = true) {
	$handle = opendir($path);
	if ($handle) {
		while (false !== ($item = readdir($handle))) {
			if ($item != "." && $item != "..")
				is_dir("$path/$item") ? delDirAndFile("$path/$item", $delDir) : unlink("$path/$item");
		}
		closedir($handle);
		if ($delDir)
			return rmdir($path);
	} else {
		if (file_exists($path)) {
			return unlink($path);
		} else {
			return FALSE;
		}
	}
}

/**
 * 压缩生成后的所有报告为zip文件
 * @param $folder
 * @param $zipFile
 * @param null $subfolder
 * @return bool
 */
function folderToZip($folder, &$zipFile, $subfolder = null) {
	if ($zipFile == null) {
		// no resource given, exit
		return false;
	}
	// we check if $folder has a slash at its end, if not, we append one
	$folder    .= end(str_split($folder)) == "/" ? "" : "/";
	$subfolder .= end(str_split($subfolder)) == "/" ? "" : "/";
	// we start by going through all files in $folder
	$handle = opendir($folder);
	while ($f = readdir($handle)) {
		if ($f != "." && $f != "..") {
			if (is_file($folder . $f)) {
				// if we find a file, store it
				// if we have a subfolder, store it there
				if ($subfolder != null)
					$zipFile->addFile($folder . $f, $subfolder . $f);
				else
					$zipFile->addFile($folder . $f);
			} elseif (is_dir($folder . $f)) {
				// if we find a folder, create a folder in the zip
				$zipFile->addEmptyDir($f);
				// and call the function again
				folderToZip($folder . $f, $zipFile, $f);
			}
		}
	}
}

$docx = new \Phpdocx\Create\CreateDocxFromTemplate('vul_template.docx');
# 数据
$text_variables = array(
	'target_company_name' => '内蒙古政府',
	'report_make_time'    => date('Y-m-d', time()),
	'project_start_time'  => '2017-08-01',
	'project_end_time'    => '2017-08-31',
	'vul_high_count'      => '40',
	'vul_middle_count'    => '90',
	'vul_low_count'       => '70',
	'vul_count'           => '100',
);

$target_data = array(
	array(
		'target_website_name' => '新安盟人民医院',
		'target_website_url'  => 'http://www.xamyy.com/',
	),
	array(
		'target_website_name' => '内蒙古自治区水利局',
		'target_website_url'  => 'http://www.nmgslw.gov.cn/',
	),
	array(
		'target_website_name' => '内蒙古自治区人民政府',
		'target_website_url'  => 'http://www.nmg.gov.cn/',
	)
);
$docx->replaceTableVariable($target_data, array('parseLineBreaks' => true));
$docx->replaceVariableByText($text_variables, array('parseLineBreaks' => true));

for ($i = 0; $i < 3; $i++) {
	$tmp_file_name = '/tmp/word/' . $tmp_save_dir_name . '/tmp_anbai_' . time() . $i . '.docx';
	$tmp           = new \Phpdocx\Create\CreateDocxFromTemplate('vul_content.docx');
	$vul_data      = array(
		'vul_title'                => '这是漏洞标题啊啊' . $i,
		'vul_type'                 => '这是漏洞类型啊啊' . $i,
		'vul_rank'                 => '这是等级啊啊' . $i,
		'vul_desc'                 => '漏洞描述啊啊' . $i,
		'vul_verification_content' => '这是漏洞验证啊啊' . $i,
		'vul_url'                  => '这是漏洞定位啊啊' . $i,
		'vul_harm'                 => '这是危害啊啊' . $i,
		'vul_suggest'              => '这是建议啊啊' . $i,
	);
	$tmp->replaceVariableByText($vul_data);
	$tmp->createDocx($tmp_file_name);
	$docx->addExternalFile(array('src' => $tmp_file_name));
}
$docx->addExternalFile(array('src' => 'vul_footer.docx'));
$docx->createDocx($tmp_save_name . '.docx');
# 图表
$docx    = new Phpdocx\Utilities\DocxUtilities();
$source  = $tmp_save_name . '.docx';
$target  = $tmp_save_name . '.docx';
$data    = array();
$data[0] = array(
	'values' => array(
		$text_variables['vul_high_count'],
		$text_variables['vul_middle_count'],
		$text_variables['vul_low_count']
	),
);
$data[0] = array(
	'title'      => '漏洞等级分布图',
	'legends'    => array(
		'高危',
		'中危',
		'低危',
	),
	'categories' => array(
		'高危',
		'中危',
		'低危',
	),
	'values'     => array(
		array($text_variables['vul_high_count']),
		array($text_variables['vul_middle_count']),
		array($text_variables['vul_low_count']),
	),
);
$docx->replaceChartData($source, $target, $data);
# 加水印
$docx   = new Phpdocx\Utilities\DocxUtilities();
$source = $tmp_save_name . '.docx';
$target = '/tmp/word/' . $tmp_save_dir_name . '/success/' . $save_name . '.docx';
$docx->watermarkDocx($source, $target, $type = 'text', $options = array('text' => '安百科技'));


$_tmp_trans = new \Phpdocx\Transform\TransformDocAdvLibreOffice();

/*
# 转为pdf
$_tmp_trans->transformDocument('/tmp/word/' . $tmp_save_dir_name . '/success/' . $save_name . '.docx', '/tmp/word/' . $tmp_save_dir_name . '/success/' . $save_name . '.pdf');

# 转为html

$_tmp_trans->transformDocument('/tmp/word/' . $tmp_save_dir_name . '/success/' . $save_name . '.docx', '/tmp/word/' . $tmp_save_dir_name . '/success/' . $save_name . '.html');
*/

//$z = new ZipArchive();
//$z->open('./' . $zip_file_name, ZIPARCHIVE::CREATE);
//folderToZip('/tmp/word/' . $tmp_save_dir_name . '/success/', $z);
//$z->close();
//
//delDirAndFile('/tmp/word/' . $tmp_save_dir_name);

